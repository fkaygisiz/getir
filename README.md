# bookseller App

## Tech stack

Java 11
Spring Boot 2.5.2
mongoDb:4.4.2
docker-compose

embeddedMongo (for tests)
swagger
postman is collection is under postman folder

## Swagger Url

http://localhost:8080/swagger-ui/index.html

Username is not important, but password should be "pwd" to get token.

## How To Run

Go to project root folder.

Then build application jar file:

```bash
./mvnw clean package -DskipTests
```

Copy jar file from target folder and go to docker folder:

```bash
cp target/bookseller-0.0.1-SNAPSHOT.jar docker
cd docker
```

And run docker compose:

```bash
docker login

docker-compose up
```

