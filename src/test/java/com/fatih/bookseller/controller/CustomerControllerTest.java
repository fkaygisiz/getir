package com.fatih.bookseller.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fatih.bookseller.configuration.TokenGenerator;
import com.fatih.bookseller.dao.CustomerRepository;
import com.fatih.bookseller.dao.OrderRepository;
import com.fatih.bookseller.dto.inputs.CustomerDto;
import com.fatih.bookseller.entity.Customer;
import com.fatih.bookseller.entity.Order;
import java.util.stream.IntStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@AutoConfigureDataMongo
@SpringBootTest
@AutoConfigureMockMvc
class CustomerControllerTest {

  private final ObjectMapper objectMapper = new ObjectMapper();
  @Autowired
  private MockMvc mvc;
  @Autowired
  private CustomerRepository customerRepository;
  @Autowired
  private OrderRepository orderRepository;
  @Autowired
  private TokenGenerator tokenGenerator;

  @AfterEach
  void cleanUp() {
    this.customerRepository.deleteAll();
    this.orderRepository.deleteAll();
  }


  @Test
  public void shouldSaveValidCustomers() throws Exception {
    String token = tokenGenerator.produceJWTToken("fatih");
    CustomerDto customerDto = new CustomerDto("fatih", "kaygisiz", "aa@bb.com");
    mvc.perform(MockMvcRequestBuilders.post("/customers").content(objectMapper.writeValueAsString(customerDto))
        .contentType(MediaType.APPLICATION_JSON).header("Authorization", token)).andExpect(status().isOk());
    Iterable<Customer> customers = this.customerRepository.findAll();
    customers.forEach(customer -> assertEquals("aa@bb.com", customer.getEmail()));
  }

  @Test
  public void shouldGetAllOrdersOfACustomerInAPaginatedWay() throws Exception {
    String token = tokenGenerator.produceJWTToken("fatih");

    Customer customer = new Customer("fatih", "kaygisiz", "aa@bb.com");
    Customer savedCustomer = this.customerRepository.save(customer);

    IntStream.rangeClosed(1, 100).forEach(i -> this.orderRepository.save(new Order(i, "book1", savedCustomer.getId())));

    mvc.perform(MockMvcRequestBuilders.get("/customers/" + savedCustomer.getId() + "/0")
        .contentType(MediaType.APPLICATION_JSON).header("Authorization", token))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(100))
        .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(5))
        .andExpect(MockMvcResultMatchers.jsonPath("$.size").value(20))
        .andExpect(MockMvcResultMatchers.jsonPath("$.content",hasSize(20)));
  }

}