package com.fatih.bookseller.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fatih.bookseller.configuration.TokenGenerator;
import com.fatih.bookseller.dao.BookRepository;
import com.fatih.bookseller.dto.inputs.BookDto;
import com.fatih.bookseller.entity.Book;
import java.math.BigDecimal;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureDataMongo
@SpringBootTest
@AutoConfigureMockMvc
class BookControllerTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private BookRepository bookRepository;

  @Autowired
  private TokenGenerator tokenGenerator;

  @AfterEach
  void cleanUp() {
    this.bookRepository.deleteAll();
  }


  @Test
  public void shouldNotAllowAccessToUnauthenticatedUsers() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/books/bookId/stock")).andExpect(status().isForbidden());
  }

  @Test
  public void shouldUpdateBookStockCount() throws Exception {
    String token = tokenGenerator.produceJWTToken("fatih");
    Book book = new Book();
    book.setName("book1");
    book.setStockCount(10);
    book.setPrice(BigDecimal.ONE);
    book.setWriter("fa ka");
    Book savedBook = this.bookRepository.save(book);
    mvc.perform(MockMvcRequestBuilders.put("/books/" + savedBook.getId() + "/200").header("Authorization", token)).andExpect(status().isOk());
    Optional<Book> updatedBook = this.bookRepository.findById(savedBook.getId());
    assertTrue(updatedBook.isPresent());
    assertEquals(200, updatedBook.get().getStockCount());
  }

  @Test
  public void shouldGetBadRequestErrorWhenValidationFailsInBookSave() throws Exception {
    String token = tokenGenerator.produceJWTToken("fatih");
    BookDto book = new BookDto();
    mvc.perform(MockMvcRequestBuilders.post("/books", book).header("Authorization", token))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void shouldGetOkWhenValidationOkInBookSave() throws Exception {
    ObjectMapper objectMapper = new ObjectMapper();

    String token = tokenGenerator.produceJWTToken("fatih");
    BookDto book = new BookDto();
    book.setName("book1");
    book.setStockCount(10);
    book.setPrice(BigDecimal.ONE);
    book.setWriter("fa ka");
    mvc.perform(MockMvcRequestBuilders.post("/books").content(objectMapper.writeValueAsString(book))
        .contentType(MediaType.APPLICATION_JSON).header("Authorization", token))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
  }

}