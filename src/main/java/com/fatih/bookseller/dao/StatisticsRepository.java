package com.fatih.bookseller.dao;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

import com.fatih.bookseller.dto.inputs.MonthlyStatistics;
import com.fatih.bookseller.entity.Order;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.DateOperators;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class StatisticsRepository {

  private final MongoTemplate mongoTemplate;

  public List<MonthlyStatistics> getMonthlyStatistics() {

    ProjectionOperation dateProjection = project()
        .and("numberOfItems").as("numberOfItems")
        .andExpression("toDecimal(orderTotal)").as("orderTotal")
        .and(DateOperators.Month.month("$orderDate")).as("month");

    Aggregation agg = newAggregation(
        dateProjection,
        group("month").count().as("totalOrderCount")
            .sum("numberOfItems").as("totalBookCount")
            .sum("orderTotal").as("totalPurchasedAmount")
        , project("totalOrderCount", "totalBookCount", "totalPurchasedAmount")
            .and("month").previousOperation()
    );

    //Convert the aggregation result into a List
    AggregationResults<MonthlyStatistics> groupResults
        = mongoTemplate.aggregate(agg, Order.class, MonthlyStatistics.class);
    return groupResults.getMappedResults();
  }
}
