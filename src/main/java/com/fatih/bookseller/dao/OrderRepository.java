package com.fatih.bookseller.dao;

import com.fatih.bookseller.entity.Order;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<Order, String> {

  Page<Order> findByCustomerId(String customerId, Pageable pageable);

  List<Order> findByOrderDateBetween(Date dateFrom, Date dateTo);
}
