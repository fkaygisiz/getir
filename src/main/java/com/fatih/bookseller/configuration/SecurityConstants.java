package com.fatih.bookseller.configuration;

public class SecurityConstants {

  public static final String HEADER = "Authorization";
  public static final String SECURITY_HEADER_PREFIX = "Bearer ";
  public static final String SECRET = "mySecretKey";
  public static final String AUTHORITIES = "authorities";
  public static final String ROLE_USER = "ROLE_USER";
  public static final String JWT_ID = "fatihJWT";
}
