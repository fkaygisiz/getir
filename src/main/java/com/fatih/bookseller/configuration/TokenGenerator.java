package com.fatih.bookseller.configuration;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

@Component
public class TokenGenerator {

  public String produceJWTToken(String username) {
    List<GrantedAuthority> grantedAuthorities = AuthorityUtils
        .commaSeparatedStringToAuthorityList(SecurityConstants.ROLE_USER);

    String token = Jwts
        .builder()
        .setId(SecurityConstants.JWT_ID)
        .setSubject(username)
        .claim(SecurityConstants.AUTHORITIES,
            grantedAuthorities.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()))
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + 600000))
        .signWith(SignatureAlgorithm.HS512,
            SecurityConstants.SECRET.getBytes()).compact();

    return SecurityConstants.SECURITY_HEADER_PREFIX + token;
  }

}
