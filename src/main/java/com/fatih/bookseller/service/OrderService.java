package com.fatih.bookseller.service;

import com.fatih.bookseller.dao.OrderRepository;
import com.fatih.bookseller.entity.Book;
import com.fatih.bookseller.entity.Customer;
import com.fatih.bookseller.entity.Order;
import com.fatih.bookseller.exceptions.ContentNotFoundException;
import com.fatih.bookseller.exceptions.NoStockException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class OrderService {

  private final OrderRepository orderRepository;

  private final BookService bookService;
  private final CustomerService customerService;
  private final Object lockObject = new Object();

  //@Transactional
  public void save(Order order) {
    synchronized (lockObject) {
      Optional<Book> book = bookService.findById(order.getBookId());
      Optional<Customer> user = customerService.findById(order.getCustomerId());
      if (book.isPresent() && user.isPresent()) {
        Book bookReal = book.get();
        if (bookReal.getStockCount() - order.getNumberOfItems() >= 0) {
          bookReal.setStockCount(bookReal.getStockCount() - order.getNumberOfItems());
          bookService.save(bookReal);
          order.setOrderDate(new Date());
          order.setOrderTotal(bookReal.getPrice().multiply(BigDecimal.valueOf(order.getNumberOfItems())));
          this.orderRepository.save(order);
        } else {
          throw new NoStockException("Not enough stock");
        }
      } else {
        throw new ContentNotFoundException("Either book or customer cannot be found");
      }
    }
  }

  public Page<Order> getAllByUserId(String customerId, Pageable pageable) {
    return orderRepository.findByCustomerId(customerId, pageable);
  }

  public Optional<Order> getById(String orderId) {
    return this.orderRepository.findById(orderId);
  }

  public List<Order> getOrdersBetween(Date dateFrom, Date dateTo) {
    return this.orderRepository.findByOrderDateBetween(dateFrom, dateTo);
  }
}
