package com.fatih.bookseller.service;

import com.fatih.bookseller.dao.CustomerRepository;
import com.fatih.bookseller.entity.Customer;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

  private final CustomerRepository customerRepository;

  public Customer save(Customer customer) {
    return customerRepository.save(customer);
  }

  public Optional<Customer> findById(String customerId) {
    return customerRepository.findById(customerId);
  }
}
