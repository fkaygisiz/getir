package com.fatih.bookseller.service;

import com.fatih.bookseller.dao.StatisticsRepository;
import com.fatih.bookseller.dto.inputs.MonthlyStatistics;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class StatisticsService {

  private final StatisticsRepository statisticsRepository;

  public List<MonthlyStatistics> getMonthlyStatistics() {
    return statisticsRepository.getMonthlyStatistics();
  }
}
