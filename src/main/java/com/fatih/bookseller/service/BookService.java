package com.fatih.bookseller.service;

import com.fatih.bookseller.dao.BookRepository;
import com.fatih.bookseller.entity.Book;
import com.fatih.bookseller.exceptions.ContentNotFoundException;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BookService {

  private final BookRepository bookRepository;

  public Book save(Book book) {
    return bookRepository.save(book);
  }

  public Optional<Book> findById(String bookId) {
    return bookRepository.findById(bookId);
  }

  public void updateStock(String bookId, int newStock) {
    Optional<Book> oBook = bookRepository.findById(bookId);
    if (oBook.isPresent()) {
      Book book = oBook.get();
      book.setStockCount(newStock);
      bookRepository.save(book);
    } else {
      throw new ContentNotFoundException("Book cannot be found");
    }
  }
}
