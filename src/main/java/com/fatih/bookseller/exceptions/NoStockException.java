package com.fatih.bookseller.exceptions;

public class NoStockException extends RuntimeException {

  public NoStockException(String message) {
    super(message);
  }
}
