package com.fatih.bookseller.exceptions;

public class ContentNotFoundException extends RuntimeException {

  public ContentNotFoundException(String message) {
    super(message);
  }
}
