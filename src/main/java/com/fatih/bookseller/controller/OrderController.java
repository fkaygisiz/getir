package com.fatih.bookseller.controller;

import com.fatih.bookseller.dto.inputs.OrderDto;
import com.fatih.bookseller.entity.Order;
import com.fatih.bookseller.service.OrderService;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@AllArgsConstructor
public class OrderController {

  private final OrderService orderService;

  @PostMapping
  public void save(@RequestBody @Valid OrderDto order) {
    orderService.save(order.getEntity());
  }

  @GetMapping("/{orderId}")
  public ResponseEntity<Order> getAllByUserId(@PathVariable String orderId) {
    Optional<Order> order = orderService.getById(orderId);
    return order.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
  }

  @GetMapping("/{dateFrom}/{dateTo}")
  public List<Order> getAllBetween(@PathVariable("dateFrom") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFrom,
      @PathVariable("dateTo") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateTo) {
    return orderService.getOrdersBetween(dateFrom, dateTo);

  }
}
