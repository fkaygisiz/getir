package com.fatih.bookseller.controller;

import com.fatih.bookseller.dto.inputs.BookDto;
import com.fatih.bookseller.entity.Book;
import com.fatih.bookseller.service.BookService;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
@AllArgsConstructor
public class BookController {

  private final BookService bookService;

  @PostMapping
  public Book save(@RequestBody @Valid BookDto book) {
    return bookService.save(book.getEntity());
  }

  @PutMapping("/{bookId}/{newStock}")
  public void save(@PathVariable String bookId, @PathVariable int newStock) {
    bookService.updateStock(bookId, newStock);
  }

}
