package com.fatih.bookseller.controller;

import com.fatih.bookseller.dto.inputs.CustomerDto;
import com.fatih.bookseller.entity.Customer;
import com.fatih.bookseller.entity.Order;
import com.fatih.bookseller.service.CustomerService;
import com.fatih.bookseller.service.OrderService;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/customers")
public class CustomerController {

  private final CustomerService customerService;
  private final OrderService orderService;

  @PostMapping
  public Customer saveUser(@RequestBody @Valid CustomerDto customer) {
    return customerService.save(customer.getEntity());
  }

  @GetMapping("/{customerId}/{pageNumber}")
  public Page<Order> getAllByUserId(@PathVariable String customerId, @PathVariable @Min(0) int pageNumber) {
    Pageable pageable = PageRequest.of(pageNumber, 20, Sort.by(Direction.DESC, "orderDate"));
    return orderService.getAllByUserId(customerId, pageable);
  }
}
