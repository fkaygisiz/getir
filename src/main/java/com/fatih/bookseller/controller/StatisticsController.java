package com.fatih.bookseller.controller;

import com.fatih.bookseller.dto.inputs.MonthlyStatistics;
import com.fatih.bookseller.service.StatisticsService;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/statistics")
@AllArgsConstructor
public class StatisticsController {

  private final StatisticsService statisticsService;

  @GetMapping
  public List<MonthlyStatistics> getMonthlyStatistics() {
    return statisticsService.getMonthlyStatistics();
  }
}
