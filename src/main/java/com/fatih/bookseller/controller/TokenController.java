package com.fatih.bookseller.controller;

import com.fatih.bookseller.configuration.TokenGenerator;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/token")
public class TokenController {

  private final TokenGenerator tokenGenerator;

  @PostMapping
  public ResponseEntity<String> getToken(@RequestParam String username, @RequestParam String password) {
    if ("pwd".equals(password)) {
      return ResponseEntity.ok(tokenGenerator.produceJWTToken(username));
    } else {
      return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
  }
}
