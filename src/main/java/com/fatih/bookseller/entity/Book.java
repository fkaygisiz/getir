package com.fatih.bookseller.entity;

import java.math.BigDecimal;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@Document("book")
@NoArgsConstructor
public class Book {

  @Id
  private String id;

  @NotEmpty(message = "Name cannot be empty")
  private String name;

  private String writer;

  @Min(value = 1, message = "stockCount must be bigger than 0")
  private int stockCount;

  @Min(value = 0, message = "price must be bigger than or equal to 0")
  @NotNull(message = "Price cannot be empty")
  private BigDecimal price;

  @Version
  private Long version;

  public Book(String name, String writer, int stockCount, BigDecimal price) {
    this.name = name;
    this.writer = writer;
    this.stockCount = stockCount;
    this.price = price;
  }
}
