package com.fatih.bookseller.entity;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("order")
@NoArgsConstructor
public class Order {

  @Id
  private String id;

  @Indexed
  private Date orderDate;

  private int numberOfItems;

  private String bookId;

  @Indexed
  private String customerId;

  private BigDecimal orderTotal;

  public Order(int numberOfItems, String bookId, String customerId) {
    this.numberOfItems = numberOfItems;
    this.bookId = bookId;
    this.customerId = customerId;
  }

  public Order(int numberOfItems, String bookId, String customerId, Date orderDate, BigDecimal orderTotal) {
    this.numberOfItems = numberOfItems;
    this.bookId = bookId;
    this.customerId = customerId;
    this.orderDate = orderDate;
    this.orderTotal = orderTotal;
  }
}
