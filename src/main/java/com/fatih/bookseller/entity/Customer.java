package com.fatih.bookseller.entity;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document("customer")
@NoArgsConstructor
public class Customer {

  @Id
  private String id;

  @NotEmpty(message = "Name cannot be empty")
  private String name;

  @NotEmpty(message = "Last name cannot be empty")
  private String lastName;

  @Email
  @Indexed(unique = true)
  @NotEmpty(message = "Email cannot be empty")
  private String email;

  public Customer(String name, String lastName, String email) {
    this.name = name;
    this.lastName = lastName;
    this.email = email;
  }
}
