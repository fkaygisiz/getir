package com.fatih.bookseller.dto.inputs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fatih.bookseller.entity.Customer;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDto {

  @NotEmpty(message = "Name cannot be empty")
  private String name;

  @NotEmpty(message = "Last name cannot be empty")
  private String lastName;

  @Email
  @NotEmpty(message = "Email cannot be empty")
  private String email;


  @JsonIgnore
  public Customer getEntity() {
    return new Customer(name, lastName, email);
  }
}
