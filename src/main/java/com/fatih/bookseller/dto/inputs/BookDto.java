package com.fatih.bookseller.dto.inputs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fatih.bookseller.entity.Book;
import java.math.BigDecimal;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BookDto {

  @NotEmpty(message = "Name cannot be empty")
  private String name;

  private String writer;

  @Min(value = 1, message = "stockCount must be bigger than 0")
  private int stockCount;

  @Min(value = 0, message = "price must be bigger than or equal to 0")
  @NotNull(message = "Price cannot be empty")
  private BigDecimal price;

  @JsonIgnore
  public Book getEntity() {
    return new Book(name, writer, stockCount, price);
  }
}
