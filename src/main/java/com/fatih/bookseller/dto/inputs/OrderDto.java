package com.fatih.bookseller.dto.inputs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fatih.bookseller.entity.Order;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderDto {

  private int numberOfItems;

  private String bookId;

  private String customerId;

  @JsonIgnore
  public Order getEntity() {
    return new Order(numberOfItems, bookId, customerId);
  }
}
