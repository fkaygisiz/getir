package com.fatih.bookseller.dto.inputs;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class MonthlyStatistics {

  private int totalOrderCount;
  private int totalBookCount;
  private BigDecimal totalPurchasedAmount;
  private String month;
}
